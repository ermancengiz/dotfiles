terminal = "konsole"
editor = os.getenv("EDITOR") or "nvim"
editor_cmd = terminal .. " -e " .. editor


modkey  = "Mod4"

bar_pos = "bottom"

cmd_rofi  = "rofi -show drun -show-icons -icon-theme MY_ICON_THEME"
cmd_dmenu = "dmenu_run -nf '#BBBBBB' -nb '#050E15' -sb '#7cb7ff' -sf '#000' -fn 'RobotoMono Nerd Font Mono:style=Bold:size=9' -p 'App :'"  



main_color_focus    = "#7CB7FF"
main_color_normal   = "#050E15"
main_color_urgent   = "#ED254E"     
main_color_white    = "#FFFFFF"     

border_color = main_color_focus
border_width = 2
