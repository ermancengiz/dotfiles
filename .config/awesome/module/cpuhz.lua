
local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local cpuhz_widget = {}

local function worker(args)


    timer = gears.timer { timeout   = 1 }
    cpuhz_widget = wibox.widget {widget = wibox.widget.textbox}


    timer:connect_signal("timeout", function()

        local file = io.open( "/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq", "r" )
        local hz = file:read( "*all" )
        file:close()

        cpuhz_widget:set_text(string.format("%s GHz" , string.format("%.2f" , hz/ 1000000 ) ))
  
    end,
      cpuhz_widget
    )


timer:start()






    return cpuhz_widget
end

return setmetatable(cpuhz_widget, { __call = function(_, ...)return worker(...)end })
