
local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local cpuload_widget = {}

local function worker(args)


    timer = gears.timer { timeout   = 1 }
    cpuload_widget = wibox.widget {widget = wibox.widget.textbox}


    timer:connect_signal("timeout", function()

        local file = io.open( "/dev/shm/cpuload.txt", "r" )
        local date = file:read( "*all" )
        file:close()

        cpuload_widget:set_text(date)  
    end,
      cpuload_widget
    )


timer:start()






    return cpuload_widget
end

return setmetatable(cpuload_widget, { __call = function(_, ...)return worker(...)end })
