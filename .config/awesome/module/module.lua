
local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local os    = require("os")
os.date('%Y-%m-%d_%H-%M')
local module_widget = {}

local function worker(args)


    timer = gears.timer { timeout   = 1 }
    widget = wibox.widget {widget = wibox.widget.textbox}


    timer:connect_signal("timeout", function()

        local file = io.open( "/file", "r" )
        local hz = file:read( "*all" )
        file:close()

        module_widget:set_text()
  
    end,
    module_widget
    )

    timer:start()


    return module_widget
end

return setmetatable(module_widget, { __call = function(_, ...)return worker(...)end })

