
local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local ram_widget = {}

local function worker(args)


    timer = gears.timer { timeout   = 1 }
    ram_widget = wibox.widget {widget = wibox.widget.textbox}


    timer:connect_signal("timeout", function()

        local file = io.open( "/dev/shm/ram.txt", "r" )
        local ram = file:read( "*all" )
        file:close()
        ram_widget:set_text(string.format("%s GB" ,ram))
    end,
      ram_widget
    )

timer:start()


    return ram_widget
end

return setmetatable(ram_widget, { __call = function(_, ...)return worker(...)end })
