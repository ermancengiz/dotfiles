-------------------------------------------------
-- Net Speed Widget for Awesome Window Manager
-- Shows current upload/download speed
-- More details could be found here:
-- https://github.com/streetturtle/awesome-wm-widgets/tree/master/net-speed-widget

-- @author Pavel Makhov
-- @copyright 2020 Pavel Makhov
-------------------------------------------------

local wibox = require("wibox")
local gears = require("gears")

local HOME_DIR = os.getenv("HOME")


local disk_speed_widget = wibox.widget {
    {
        id = 'rx_speed',
        widget = wibox.widget.textbox
    },
    {
        id = 'tx_speed',
        widget = wibox.widget.textbox
    },
    layout = wibox.layout.fixed.horizontal,
    set_rx_text = function(self, new_rx_speed)
        self:get_children_by_id('rx_speed')[1]:set_text(tostring(new_rx_speed))
    end,
    set_tx_text = function(self, new_tx_speed)
        self:get_children_by_id('tx_speed')[1]:set_text(tostring(new_tx_speed))
    end
}

local prev_rx = 0
local prev_tx = 0

local function convert_to_h(bytes)
    local speed
    local dim

    local megabytes = bytes / 2048 -- 1 048 576
        
    speed = string.format("%03.0f" , megabytes)
    
    return speed .. " " 
end

local function split(string_to_split, separator)
    if separator == nil then separator = "%s" end
    local t = {}

    for str in string.gmatch(string_to_split, "([^".. separator .."]+)") do
        table.insert(t, str)
    end

    return t
end

local function worker(args)


    
    timer = gears.timer { timeout   = 1 }


    timer:connect_signal("timeout", function()

        local file = io.open( "/sys/block/sdb/stat", "r" )
        local ssdstat = file:read( "*all" )
        file:close()
        
        local cur_vals = split(ssdstat, ' ') 
        local cur_rx = 0
        local cur_tx = 0

          cur_rx = cur_rx + cur_vals[3] 
          cur_tx = cur_tx + cur_vals[7] 
        

        local speed_rx = cur_rx - prev_rx
        local speed_tx = cur_tx - prev_tx

        disk_speed_widget:set_rx_text(convert_to_h(speed_rx))
        disk_speed_widget:set_tx_text(convert_to_h(speed_tx))

        prev_rx = cur_rx
        prev_tx = cur_tx
  
    end,
    disk_speed_widget
    )

    timer:start()

    return disk_speed_widget

end

return setmetatable(disk_speed_widget, { __call = function(_, ...) return worker(...) end })
