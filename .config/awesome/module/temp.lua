
local wibox = require("wibox")
local gears = require("gears")
local temp_widget = {}

local function worker(args)


    timer = gears.timer { timeout   = 1 }
    temp_widget = wibox.widget { widget = wibox.widget.textbox}

    timer:connect_signal("timeout", function()

        local file = io.open( "/sys/class/thermal/thermal_zone0/temp", "r" )
        local temp = file:read( "*all" )
        file:close()

        temp_widget:set_text(string.format("%s°C" , math.floor(temp/1000)))
  
    end,
    temp_widget
    )


    timer:start()

    return temp_widget
end

return setmetatable(temp_widget, { __call = function(_, ...)return worker(...)end })
