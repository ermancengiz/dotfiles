-------------------------------------------------
-- Brightness Widget for Awesome Window Manager
-- Shows the brightness level of the laptop display
-- More details could be found here:
-- https://github.com/streetturtle/awesome-wm-widgets/tree/master/widget-widget

-- @author Pavel Makhov
-- @copyright 2019 Pavel Makhov
-------------------------------------------------

local wibox = require("wibox")
local watch = require("awful.widget.watch")
local spawn = require("awful.spawn")
local beautiful = require("beautiful")

local GET_BRIGHTNESS_CMD = [[
  sh -c "/usr/bin/brightnessctl -d intel_backlight | grep 'Current' | cut -d' ' -f 4 | tr -d '()%'
"]] 

local DEC_BRIGHTNESS_CMD = 'sh -c "/usr/bin/brightnessctl -d intel_backlight set 10%- "'
local INC_BRIGHTNESS_CMD = 'sh -c "/usr/bin/brightnessctl -d intel_backlight set 10%+"'


local naughty = require("naughty")

local function exec_print(s)
  
  naughty.notify({ preset = naughty.config.presets.normal,
                     title = "OUTPUT :",
                     text = tostring(s) })
end





local widget = {}

local function worker(args)




    local args = args or {}

    local get_brightness_cmd = args.get_brightness_cmd or GET_BRIGHTNESS_CMD
    local inc_brightness_cmd = args.inc_brightness_cmd or INC_BRIGHTNESS_CMD
    local dec_brightness_cmd = args.dec_brightness_cmd or DEC_BRIGHTNESS_CMD
    local color = args.color or beautiful.fg_color
    local bg_color = args.bg_color or '#ffffff11'
    local timeout = args.timeout or 1


    widget = wibox.widget {
        max_value = 1,
        thickness = 2,
        start_angle = 4.71238898, -- 2pi*3/4
        forced_height = 18,
        forced_width = 18,
        bg = bg_color,
        paddings = 2,
        colors = {"#7cb7ff"},
        widget = wibox.container.arcchart
    }



    local update_widget = function(widget, stdout)
      
      local brightness_level = string.match(stdout, "(%d?%d?%d?)")
      brightness_level = tonumber(string.format("% 3d", brightness_level))
    
      widget.value = brightness_level / 100;
   
    end,

    widget:connect_signal("button::press", function(_, _, _, button)
      if (button == 4) then
        spawn(inc_brightness_cmd, false)
      elseif (button == 5) then
        spawn(dec_brightness_cmd, false)
      end
    end)

    watch(get_brightness_cmd, timeout, update_widget, widget)


    return widget
  end

  return setmetatable(widget, { __call = function(_, ...)
    return worker(...)
  end })
