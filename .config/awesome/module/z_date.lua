
local wibox = require("wibox")
local gears = require("gears")
local date_widget = {}


local function worker(args)

    timer = gears.timer { timeout   = 1 }
    date_widget = wibox.widget { widget = wibox.widget.textbox}


timer:connect_signal("timeout", function()

    local file = io.open( "/dev/shm/date.txt", "r" )
    local date = file:read( "*all" )
    file:close()

    date_widget:set_text(date)
  
  end,
  date_widget
)


timer:start()
 
    return date_widget
end

return setmetatable(date_widget, { __call = function(_, ...)return worker(...)end })
