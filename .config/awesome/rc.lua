local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
local wibox = require("wibox")
local beautiful = require("beautiful")
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
require("awful.hotkeys_popup.keys")


require("debug")
require("key")
require("rules")
require("config")
require("bar")
require("titlebar")

beautiful.init(string.format("%s/.config/awesome/themes/default/theme.lua", os.getenv("HOME")))

awful.spawn.single_instance("nitrogen --restore")  
awful.spawn("sh " .. string.format("%s/python/startup.sh", os.getenv("HOME")))  

client.connect_signal("manage", function (c)

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        awful.placement.no_offscreen(c)
    end
end)

client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
