local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local naughty = require("naughty")
local beautiful = require("beautiful")
local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()

local theme = {}

--theme.font          = "sans 8"
theme.font          = "Roboto Medium 9"
theme.taglist_font  = "Roboto Bold 9"

main_color_focus    = "#7CB7FF"
main_color_normal   = "#050E15"
main_color_urgent   = "#ED254E"     
main_color_white    = "#FFFFFF"     

theme.bg_systray    = main_color_normal

-- border 
theme.useless_gap   = dpi(1)
theme.border_width  = dpi(1)
theme.border_normal = main_color_normal
theme.border_focus  = main_color_focus
theme.border_marked = main_color_urgent

-- titlebar 
theme.titlebar_bg_normal    = main_color_normal
theme.titlebar_fg_normal    = main_color_white
theme.titlebar_bg           = main_color_normal
theme.titlebar_fg           = main_color_white

--- menu 
theme.menu_bg_normal    = main_color_normal
theme.menu_fg_normal    = main_color_white
theme.menu_fg_focus     = main_color_normal
theme.menu_bg_focus     = main_color_focus

theme.bg_normal            = main_color_normal   
theme.xbackground          = main_color_normal
                        
theme.taglist_bg_focus     = main_color_focus
theme.taglist_fg_focus     = main_color_normal
                        
theme.taglist_bg_empty     = main_color_normal    
theme.taglist_fg_empty     = "#282c34"     
                        
theme.taglist_bg_occupied  = main_color_normal   
theme.taglist_fg_occupied  = main_color_focus
                        
theme.taglist_bg_urgent    = main_color_normal    
theme.taglist_fg_urgent    = main_color_urgent 


theme.menu_height = dpi(15)
theme.menu_width  = dpi(100)

theme.titlebar_close_button_normal              = themes_path.."default/titlebar/close_normal.png"
theme.titlebar_close_button_focus               = themes_path.."default/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal           = themes_path.."default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus            = themes_path.."default/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive     = themes_path.."default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive      = themes_path.."default/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active       = themes_path.."default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active        = themes_path.."default/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive    = themes_path.."default/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive     = themes_path.."default/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active      = themes_path.."default/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active       = themes_path.."default/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive  = themes_path.."default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive   = themes_path.."default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active    = themes_path.."default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active     = themes_path.."default/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = themes_path.."default/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path.."default/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active   = themes_path.."default/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active    = themes_path.."default/titlebar/maximized_focus_active.png"

theme.wallpaper = themes_path.."default/background.png"

-- You can use your own layout icons like this:
theme.layout_fairh      = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv      = themes_path.."default/layouts/fairvw.png"
theme.layout_floating   = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier  = themes_path.."default/layouts/magnifierw.png"
theme.layout_max        = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile       = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop    = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral     = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle    = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw   = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne   = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw   = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse   = themes_path.."default/layouts/cornersew.png"

-- Generate Awesome icon:




naughty.config.padding                          = 8
naughty.config.spacing                          = 9
naughty.config.timeout                          = 5
naughty.config.defaults.fg                      = "#FFF"
naughty.config.defaults.bg                      = "#00000080"
naughty.config.defaults.border_width            = 2
naughty.config.defaults.margin                  = 9

naughty.config.defaults.height                  = 110
naughty.config.defaults.width                   = 450


naughty.config.defaults.position                = "bottom_right"


naughty.config.presets.normal                   
= { 
    bg            = "#00000080",
    fg            = "#FFFFFF",
    border_width  = 1,
    margin        = 12,
    icon_size     = 85,
     
}


-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = "/usr/share/icons/hicolor/Arc/"


 







return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
